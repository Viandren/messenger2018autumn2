/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.services;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.domain.MessageCreator;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author peti
 */
@Service
public class MessageService {
    
    
    public List<Message> findAllMessages(){
        return MessageCreator.getMessages();
    }
    
    public List<Message> findMessages(
            Long id, String author, String text,
            LocalDateTime from, LocalDateTime to,
            String orderBy, Boolean asc){
        Comparator<Message> comp = null;
        if(("id").equals(orderBy)) comp = Comparator.comparing(m -> m.getId());
        else if(("author").equals(orderBy)) comp = Comparator.comparing(m -> m.getAuthor());
        else if(("text").equals(orderBy)) comp = Comparator.comparing(m -> m.getText());
        else if(("createDate").equals(orderBy)) comp = Comparator.comparing(m -> m.getCreateDate());
        if(comp != null && asc != null && ! asc){
            comp = comp.reversed();
        }
        Stream<Message> stream = MessageCreator.getMessages().stream()
                .filter(m -> id == null ? true : m.getId().equals(id))
                .filter(m -> StringUtils.isEmpty(author) ? true : m.getAuthor().contains(author))
                .filter(m -> StringUtils.isEmpty(text) ? true : m.getText().contains(text))
                .filter(m -> from == null ? true : m.getCreateDate().isAfter(from))
                .filter(m -> to == null ? true : m.getCreateDate().isBefore(to));
        if(comp != null){
            stream = stream.sorted(comp);
        }
        return stream.collect(Collectors.toList());
    }
    
    public Message findMessageById(Long id){
        Optional<Message> mess = MessageCreator.getMessages()
                .stream()
                .filter(m -> m.getId().equals(id))
                .findFirst();
        return mess.orElse(null);
    }
    
    @PreAuthorize("hasAuthority('ADMIN')")
    public boolean deleteMessage(Long id){
        ListIterator<Message> li = MessageCreator.getMessages().listIterator();
        while (li.hasNext()) {
            Message next = li.next();
            if(next.getId().equals(id)){
                li.remove();
                return true;
            }
            
        }
        return false;
    }
    
    public void createMessage(Message m){
        MessageCreator.addMessage(m);
    }
}
