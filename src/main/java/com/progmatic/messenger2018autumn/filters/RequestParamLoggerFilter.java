/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author peti
 */
@Component
public class RequestParamLoggerFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(RequestParamLoggerFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOG.trace("do filter called");
        request.getParameterMap()
                .forEach((name, val) ->  LOG.trace("parameter name: {}, value:{}", name, val));
        chain.doFilter(request, response);

    }

}
