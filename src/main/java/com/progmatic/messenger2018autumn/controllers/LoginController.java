/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.domain.User;
import com.progmatic.messenger2018autumn.dto.SubscriptionDTO;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author peti
 */
@Controller
public class LoginController {
    
    UserDetailsManager userDetailsManager;

    @Autowired
    public LoginController(UserDetailsService userDetailsManager) {
        this.userDetailsManager = (UserDetailsManager) userDetailsManager;
    }       
    
    @GetMapping(path = "/login")
    public String login(){
        return "login";
    }
    
    @GetMapping("/subscribe")
    public String showSubscribe(@ModelAttribute("subs") SubscriptionDTO sdto){
        return "subscribe";
    }
    
    @PostMapping("/subscribe")
    public String doSubscribe(@Valid @ModelAttribute("subs") SubscriptionDTO sdto, BindingResult errors){
        if(errors.hasErrors() || !checkSubscription(sdto, errors)){
            return "subscribe";
        }
        User user = new User(sdto.getName(), sdto.getPassword(), "USER");
        userDetailsManager.createUser(user);
        return "redirect:/login";
    }

    private boolean checkSubscription(SubscriptionDTO sdto, BindingResult errors) {
        boolean ok = true;
        if(!sdto.getPassword().equals(sdto.getPassword2())){
            FieldError error = new FieldError("subs", "password", "A két jelszó nem egyezik meg!");
            errors.addError(error);
            FieldError error2 = new FieldError("subs", "password2", "A két jelszó nem egyezik meg!");
            errors.addError(error2);
            ok = false;
        }
        if(userDetailsManager.userExists(sdto.getName())){
            FieldError error = new FieldError("subs", "name", "Ezzel a névvel már létezik felhasználó. Válassz másik nevet!");
            errors.addError(error);
            ok = false;
        }
        return ok;
    }
    
    
}
