/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.domain;

import com.progmatic.messenger2018autumn.utils.DateConstants;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author peti
 */
public class Message {

    private Long id;

    private String author;
    
    @NotBlank
    private String text;
    
    @DateTimeFormat(pattern = DateConstants.DATETIME_PATTERN)
    private LocalDateTime createDate;

    public Message(Long id, String author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
        this.createDate = LocalDateTime.now();
    }

    public Message() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
